# UI Developer – Screening Test
#### Create a portal for farmer having registration, login and profile pages. 
The data needed to be captured and displayed after login
*   Name
* 	Email
* 	Aadhaar No
* 	Kissan Id
* 	Gender
* 	State
* 	District
* 	Taluk
* 	Village
* 	Photo
* 	Username
* 	Password
* 	Date of Birth
	
## Evaluation will be based on:
1.	UI & UX
2.	Working Demo

## Submission
1.	Submission of the project will be through GitLab:
    1. Clone the repo:  https://gitlab.com/KeralaBlockchainAcademy/screening_test. 
    2. Create a **branch with your name** and push to that branch.
    3. The **folder present in the branch should be your name** with all source code in it.
    4. If needed, include instructions for executing code.
2.	Submission on or before **29 Aug, 2019**.
